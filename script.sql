CREATE TABLE `puestos` (
  `nombre` VARCHAR(50) NULL,
  `empresa` VARCHAR(50) NULL,
  `lugar` VARCHAR(50) NULL,
  `tiempo` VARCHAR(50) NULL,
  `contrato` VARCHAR(50) NULL,
  `salario` VARCHAR(50) NULL,
  `descripcion` VARCHAR(1500) NULL,
  `fechaInicio` VARCHAR(50) NULL);