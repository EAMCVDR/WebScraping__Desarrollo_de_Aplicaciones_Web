import bs4 as bs
import urllib.request
import MySQL as dB
import time
import datetime

main_url = ''
# listaInfo = []
lista_palabras_clave = ['desarrollador', 'developer', 'programador', 'programmer', 'TI',
                        'IT', 'QA', 'computadora', 'computer',
                        'probador', 'tester', 'software']

ITEM = dB.MySQL('puestos')


# Filtro para evitar almacenar registros que no estén relacionados con TI
def filtro(val):
    global lista_palabras_clave
    for i in lista_palabras_clave:
        if i in val:
            return True
    return False


# Trata de parsear una dirección web para que sea válida (convierte rutas relativas en rutas exactas)
def get_url(url):
    if url[0] == '/':
        temp = main_url.split('?')[0].rstrip('/')
        return temp + url
    elif url[0] != '#':
        return url
    else:
        raise ValueError


# Función para obtener toda la información de una dirección web
def read_page(url):
    global main_url
    if url[0] == '/' and len(url) > 2:
        source_text = urllib.request.urlopen(main_url + url).read()
    else:
        source_text = urllib.request.urlopen(url).read()
    soup = bs.BeautifulSoup(source_text, 'lxml')
    return soup


# Función principal del scraping que recorre todas las páginas de ofertas de trabajo
def scraping(first_url, append_url):
    # Vinculación con las variable globales, para almacenar y consumir datos
    global main_url

    # Captura e impresión del tiempo en que comienza el scraping
    tiempo_inicial = time.time()
    print('[' + str(
        datetime.datetime.fromtimestamp(tiempo_inicial).strftime('%Y-%m-%d %H:%M:%S')) + '] Empezando Scraping...')

    # parseo de las direcciones dadas inicialmente
    main_url = first_url.rstrip('/')
    append_url = '/' + append_url.strip('/') + '/'

    source = read_page(append_url)

    # Inicialización de variables para su uso
    cuenta_total = 0
    cuenta_fallos = 0
    index = 1
    while source.header.h1.string != "No se ha encontrado ofertas de trabajo con los filtros actuales":
        cont = 0
        cont_errores = 0

        print("\nPágina " + str(index))

        lista_puestos = source.find('section', {'class': 'cm-9 parrilla_oferta lO'}).find_all('div', {'class': 'iO'})
        total_elementos = len(lista_puestos)

        # Recorrido de las ofertas presentes en la página # index
        for i in range(total_elementos):
            print('\rElementos [' + chr(9610) * i + '_' * (total_elementos - i - 1) + ']', end='', flush=True)
            try:
                temp = get_info(lista_puestos[i].a['href'])
                temp.fechaInicio = lista_puestos[i].find('span', {'class': 'dO'}).string
                if filtro(temp.descripcion):
                    cont += 1
                    # listaInfo.append(temp)
                    # ----------Conexión e inserción en la base de datos
                    query = """ insert into puestos values ('{}','{}','{}','{}','{}','{}','{}','{}')""".format(
                        temp.nombrePuesto,
                        temp.empresa,
                        temp.lugar, temp.tiempo,
                        temp.tipoContrato,
                        temp.salario,
                        temp.descripcion[0:1195],
                        temp.fechaInicio)
                    ITEM.post(query)
                    # -------------------------------------------------
            except:
                cont_errores += 1
        # -------------------------------------------
        print('\nElementos Agregados: ' + str(cont) + ' | Errores Cometidos: ' + str(cont_errores))
        index += 1
        cuenta_total += cont
        cuenta_fallos += cont_errores
        source = read_page(append_url + "?p=" + str(index))
    tiempo_final = time.time()
    print('\n\n[' + str(
        datetime.datetime.fromtimestamp(tiempo_final).strftime('%Y-%m-%d %H:%M:%S')) + '] Finalización de Scraping')
    print('*______________________________________________\n|')
    print('| Duración: ' + str(datetime.timedelta(seconds=(tiempo_final - tiempo_inicial))))
    print('| Cantidad de elementos agregados: ' + str(cuenta_total))
    print('| Cantidad de fallos presentes: ' + str(cuenta_fallos))
    print('|\n*______________________________________________')


# Clase que contiene los datos de un elemento (estructura)
class Puesto:
    def __init__(self):
        self.nombrePuesto = ""
        self.empresa = ""
        self.lugar = ""
        self.tiempo = ""
        self.tipoContrato = ""
        self.salario = ""
        self.descripcion = ""
        self.fechaInicio = ""


# Función que obtiene, estructura y devuelve la información de un puesto de trabajo
def get_info(url):
    page = read_page(url)
    text = page.find("section", {'class': 'box box_r'})
    attr = text.find_all('li')
    lista = {'Puesto': '', 'Empresa': '', 'Localización': '', 'Jornada': '', 'Tipo de contrato': '', 'Salario': ''}

    # Por cada  elemento li obtiene la llave y el valor, para agregarlo al diccionario lista
    for i in range(len(attr) - 1):
        temp = str(attr[i].p.string)
        temp = temp.replace("\r", '')
        temp = temp.replace("\n", '')
        temp = temp.replace("\t", '')
        temp = temp.strip(" ")
        try:
            key = attr[i].h3.string
        except:
            key = 'Puesto'
        lista[key] = temp
    data = Puesto()
    data.nombrePuesto = lista['Puesto']
    data.empresa = lista['Empresa']
    data.lugar = lista['Localización']
    data.tiempo = lista['Jornada']
    data.tipoContrato = lista['Tipo de contrato']
    data.salario = lista['Salario']

    text = page.find('div', {'class': "cm-12 box_i bWord"})
    data.descripcion = text.li.text
    return data

scraping('https://www.computrabajo.co.cr', 'ofertas-de-trabajo')

